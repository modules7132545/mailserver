resource "helm_release" "mail-server" {
  name       = "mail-server"
  repository = "https://dysnix.github.io/charts"
  chart      = "raw"
  namespace  = local.namespace
  version    = "v0.3.2"
  values = [
    yamlencode({
      resources = [
        {
          apiVersion = "cert-manager.io/v1"
          kind       = "Certificate"
          metadata = {
            name      = "mail"
            namespace = local.namespace
          }
          spec = {
            secretName = local.tls-secret-name
            additionalOutputFormats = [{
              type = "CombinedPEM"
            }]
            issuerRef = {
              name  = var.cluster-issuer
              kind  = "ClusterIssuer"
              group = "cert-manager.io"
            }
            dnsNames = [for domain in var.domains : domain.name]
          }
        }
      ]
    })
  ]
}
