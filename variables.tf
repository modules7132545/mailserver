variable "name" {
  default = "mail-server"
}

variable "base_domain" {
  type = string
}

variable "domains" {
  type = list(object({
    name     = string,
    selector = string
  }))
}

variable "kubernetes" {
  type = object({
    config_path    = string
    config_context = string
  })
}

variable "vault" {
  type = object({
    host  = string
    token = string
  })
}

variable "mail" {
  type = object({
    AMAVISD_DB_PASSWORD              = string
    FIRST_MAIL_DOMAIN_ADMIN_PASSWORD = string
    MLMMJADMIN_API_TOKEN             = string
    ROUNDCUBE_DES_KEY                = string
  })
}

variable "cluster-issuer" {
  type = string
}

variable "cloudflare" {
  type = object({
    email    = string
    password = string
    token    = string
    zone     = map(string)
  })
}

variable "cluster" {
  type = object({
    name  = string
    hosts = map(string)
  })
}
