resource "kubernetes_namespace_v1" "mail-server" {
  metadata {
    name = "mail-server"
  }
}

resource "kubernetes_service_v1" "mail-server" {
  metadata {
    name      = local.component
    namespace = var.name
    labels = {
      "service" = local.component
    }
  }
  spec {
    type = "LoadBalancer"
    selector = {
      "app" = local.component
    }
    dynamic "port" {
      for_each = local.ports
      content {
        port        = port.value.number
        target_port = port.value.name
        name        = port.value.name
        node_port   = can(port.value.node_port) ? port.value.node_port : null
      }
    }
  }
}

resource "kubernetes_service_v1" "mail-server-ingress" {
  metadata {
    name      = "mail-server-ingress"
    namespace = local.namespace
    labels = {
      "service" = "mail-server-ingress"
    }
  }
  spec {
    type = "ClusterIP"
    selector = {
      "app" = local.component
    }
    dynamic "port" {
      for_each = local.ingress-ports
      content {
        port        = port.value.number
        target_port = port.value.name
        name        = port.value.name
      }
    }
  }
}

resource "kubernetes_ingress_v1" "mail-server" {
  metadata {
    name      = var.name
    namespace = local.namespace
    annotations = {
      "cert-manager.io/cluster-issuer"               = var.cluster-issuer
      "nginx.ingress.kubernetes.io/backend-protocol" = "HTTPS"
    }
  }
  spec {
    ingress_class_name = "nginx"
    tls {
      hosts       = [data.vault_generic_secret.mail.data["HOSTNAME"]]
      secret_name = "${data.vault_generic_secret.mail.data["HOSTNAME"]}-tls"
    }
    rule {
      host = data.vault_generic_secret.mail.data["HOSTNAME"]
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = kubernetes_service_v1.mail-server-ingress.metadata[0].name
              port {
                name = "https"
              }
            }
          }
        }
      }
    }
  }
}
