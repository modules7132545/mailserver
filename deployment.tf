resource "kubernetes_deployment_v1" "mail-server" {
  depends_on = [kubernetes_job.dkim]
  metadata {
    name      = local.component
    namespace = var.name
    labels = {
      "app" = local.component
    }
  }
  spec {
    selector {
      match_labels = {
        "app" = local.component
      }
    }
    replicas = 1
    template {
      metadata {
        labels = {
          "app" = local.component
        }
      }
      spec {
        container {
          image = "iredmail/mariadb:stable"
          name  = "iredmail"
          dynamic "port" {
            for_each = concat(local.ports, local.ingress-ports)
            content {
              container_port = port.value.number
              name           = port.value.name
            }
          }
          env_from {
            config_map_ref {
              name = var.name
            }
          }
          env_from {
            secret_ref {
              name = var.name
            }
          }
          dynamic "volume_mount" {
            for_each = local.claims
            content {
              name       = volume_mount.value.name
              mount_path = volume_mount.value.path
            }
          }
          volume_mount {
            name       = "clamfail"
            mount_path = "/etc/supervisor/conf.d/clamav.conf"
            sub_path   = "clamav.conf"
          }
          volume_mount {
            name       = "amavis"
            mount_path = "/etc/amavis/conf.d/50-user"
            sub_path   = "50-user"
          }
          volume_mount {
            name       = "dkim"
            mount_path = "/opt/iredmail/custom/amavisd/dkim"

            read_only = false
          }
          dynamic "volume_mount" {
            for_each = local.tls
            content {
              name       = volume_mount.value.name
              mount_path = volume_mount.value.mount_path
              sub_path   = volume_mount.value.path
              read_only  = false
            }
          }
        }
        dynamic "volume" {
          for_each = local.claims
          content {
            name = volume.value.name
            persistent_volume_claim {
              claim_name = volume.value.name
            }
          }
        }
        volume {
          name = "amavis"
          config_map {
            name = "amavis"
            items {
              key  = "amavis"
              path = "50-user"
            }
          }
        }
        volume {
          name = "clamfail"
          config_map {
            name = var.name
            items {
              key  = "clamav"
              path = "clamav.conf"
            }
          }
        }
        volume {
          name = "dkim"
          persistent_volume_claim {
            claim_name = "dkim"
          }
        }
        dynamic "volume" {
          for_each = local.tls
          content {
            name = volume.value.name
            secret {
              secret_name = local.tls-secret-name
              items {
                key  = volume.value.key
                path = volume.value.path
              }
            }
          }
        }
      }
    }
  }
}
