resource "vault_kv_secret_v2" "cloudflare" {
  mount = vault_mount.mail.path
  name  = "cloudflare"
  data_json = jsonencode(
    var.cloudflare
  )
}

data "vault_generic_secret" "cloudflare" {
  path       = "${vault_mount.mail.path}/${vault_kv_secret_v2.cloudflare.name}"
  depends_on = [vault_kv_secret_v2.cloudflare]
}

resource "cloudflare_record" "sfp" {
  zone_id = jsondecode(data.vault_generic_secret.cloudflare.data["zone"]).default
  name    = "@"
  type    = "TXT"
  value   = "v=spf1 +a +mx ~all"
  ttl     = 1
}

resource "cloudflare_record" "dmark" {
  zone_id = jsondecode(data.vault_generic_secret.cloudflare.data["zone"]).default
  name    = "_dmarc"
  type    = "TXT"
  value   = "v=DMARC1; p=reject; rua=mailto:${data.vault_generic_secret.mail.data["POSTMASTER_EMAIL"]}; ruf=mailto:${data.vault_generic_secret.mail.data["POSTMASTER_EMAIL"]}; adkim=s; aspf=s; fo=1; sp=reject"
  ttl     = 1
}

resource "cloudflare_record" "dkim" {
  depends_on = [null_resource.fetch_files]
  for_each = {
    for domain in var.domains : domain.selector => {
      file_content = file(".cache/${domain.selector}.txt")
      name         = domain.name
    }
  }
  zone_id = jsondecode(data.vault_generic_secret.cloudflare.data["zone"]).default
  name    = format("%s%s%s", regex("^(.+?)_domainkey", each.value.file_content)[0], "_domainkey.", each.value.name)
  type    = "TXT"
  ttl     = 1
  value   = format("%s%s", "v=DKIM1; k=rsa; p=", regex("p=(.+?)\"", each.value.file_content)[0])
}

resource "cloudflare_record" "MX" {
  zone_id  = jsondecode(data.vault_generic_secret.cloudflare.data["zone"]).default
  name     = "@"
  type     = "MX"
  value    = data.vault_generic_secret.mail.data["HOSTNAME"]
  priority = 5
}

resource "vault_kv_secret_v2" "cluster" {
  mount = vault_mount.mail.path
  name  = "cluster"
  data_json = jsonencode(
    var.cluster
  )
}

data "vault_generic_secret" "cluster" {
  path       = "${vault_mount.mail.path}/${vault_kv_secret_v2.cluster.name}"
  depends_on = [vault_kv_secret_v2.cluster]
}

# resource "cloudflare_record" "xmpp-client" {
#   for_each = var.cluster.hosts
#   zone_id  = jsondecode(data.vault_generic_secret.cloudflare.data["zone"]).default
#   name     = "_xmpp-client._tcp.default.ru."
#   type     = "SRV"
#   value    = "0 5 5222 ${each.value}"
# }

# resource "cloudflare_record" "xmpp-server" {
#   for_each = var.cluster.hosts
#   zone_id  = jsondecode(data.vault_generic_secret.cloudflare.data["zone"]).default
#   name     = "_xmpp-server._tcp.default.ru."
#   type     = "SRV"
#   value    = "0 5 5269 ${each.value}"
# }


resource "cloudflare_record" "autoconfig" {
  for_each = var.cluster.hosts
  zone_id  = jsondecode(data.vault_generic_secret.cloudflare.data["zone"]).default
  name     = "autoconfig"
  type     = "A"
  value    = each.value
}

resource "cloudflare_record" "autodiscover" {
  for_each = var.cluster.hosts
  zone_id  = jsondecode(data.vault_generic_secret.cloudflare.data["zone"]).default
  name     = "autodiscover"
  type     = "A"
  value    = each.value
}
