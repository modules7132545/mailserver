provider "kubectl" {
  config_path    = var.kubernetes.config_path
  config_context = var.kubernetes.config_context
}

provider "vault" {
  address = var.vault.host
  token   = var.vault.token
}

provider "kubernetes" {
  config_path    = var.kubernetes.config_path
  config_context = var.kubernetes.config_context
}

provider "cloudflare" {
  api_token = var.cloudflare.token
}

provider "null" {}

provider "helm" {
  kubernetes {
    config_path    = var.kubernetes.config_path
    config_context = var.kubernetes.config_context
  }
}
