resource "vault_mount" "mail" {
  path        = "mail"
  type        = "kv"
  options     = { version = "2" }
  description = "mail mount data"
}

locals {
  namespace = kubernetes_namespace_v1.mail-server.metadata[0].name
  service   = kubernetes_service_v1.mail-server.metadata[0].name
}

resource "vault_kv_secret_v2" "mail" {
  mount = vault_mount.mail.path
  name  = var.base_domain
  data_json = jsonencode(
    {
      AMAVISD_DB_PASSWORD              = var.mail.AMAVISD_DB_PASSWORD
      FIRST_MAIL_DOMAIN_ADMIN_PASSWORD = var.mail.FIRST_MAIL_DOMAIN_ADMIN_PASSWORD
      MLMMJADMIN_API_TOKEN             = var.mail.MLMMJADMIN_API_TOKEN
      ROUNDCUBE_DES_KEY                = var.mail.ROUNDCUBE_DES_KEY
      "FIRST_MAIL_DOMAIN"              = var.base_domain
      "HOSTNAME"                       = "mail.${var.base_domain}"
      "POSTMASTER_EMAIL"               = "postmaster@${var.base_domain}"
      "implicit-host"                  = "${local.service}.${local.namespace}"
    }
  )
}

data "vault_generic_secret" "mail" {
  depends_on = [vault_kv_secret_v2.mail]
  path       = "${vault_mount.mail.path}/${var.base_domain}"
}

resource "kubernetes_config_map_v1" "mail-server" {
  metadata {
    name      = var.name
    namespace = var.name
  }
  data = {
    "HOSTNAME"          = data.vault_generic_secret.mail.data["HOSTNAME"]
    "FIRST_MAIL_DOMAIN" = data.vault_generic_secret.mail.data["FIRST_MAIL_DOMAIN"]
    "POSTMASTER_EMAIL"  = data.vault_generic_secret.mail.data["POSTMASTER_EMAIL"]
    //disable clamav
    "clamav" = ""
  }
}

resource "kubernetes_secret_v1" "mail-server" {
  metadata {
    name      = var.name
    namespace = var.name
  }
  data = {
    "FIRST_MAIL_DOMAIN_ADMIN_PASSWORD" = data.vault_generic_secret.mail.data["FIRST_MAIL_DOMAIN_ADMIN_PASSWORD"]
    "MLMMJADMIN_API_TOKEN"             = data.vault_generic_secret.mail.data["MLMMJADMIN_API_TOKEN"]
    "ROUNDCUBE_DES_KEY"                = data.vault_generic_secret.mail.data["ROUNDCUBE_DES_KEY"]
    "AMAVISD_DB_PASSWORD"              = data.vault_generic_secret.mail.data["AMAVISD_DB_PASSWORD"]
  }
}

locals {
  tls-secret-name = "mail-tls"
}

locals {
  component = "mail-server"
}
