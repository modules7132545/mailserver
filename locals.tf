locals {
  mount_path = "/opt/iredmail/ssl/"
  tls = tolist([
    {
      name       = "combined"
      key        = "tls-combined.pem"
      path       = "combined.pem"
      mount_path = "${local.mount_path}combined.pem"
    },
    {
      name       = "key"
      key        = "tls.key"
      path       = "key.pem"
      mount_path = "${local.mount_path}key.pem"
    },
    {
      name       = "crt"
      key        = "tls.crt"
      path       = "cert.pem"
      mount_path = "${local.mount_path}cert.pem"
    },
  ])
  claims = tolist([
    {
      name    = "vmail"
      storage = "7Gi"
      path    = "/var/vmail"
    },
    {
      name    = "mysql"
      storage = "5Gi"
      path    = "/var/lib/mysql"
    },
    {
      name    = "clamav"
      storage = "2Gi"
      path    = "/var/lib/clamav"
    },
    {
      name    = "spamassassin"
      storage = "2Gi"
      path    = "/var/lib/spamassassin"
    },
    {
      name    = "postfix"
      storage = "2Gi"
      path    = "/var/spool/postfix"
    },
    # {
    #   name    = "custom"
    #   storage = "1Gi"
    #   path    = "/opt/iredmail/custom"
    # },
  ])
  ingress-ports = tolist([
    {
      number = 80
      name   = "http"
    },
    {
      number = 443
      name   = "https"
    },
  ])
  ports = tolist([
    {
      number = 110
      name   = "pop3-tls"
    },
    {
      number = 995
      name   = "pop3-ssl"
    },
    {
      number = 143
      name   = "imap-tls"
    },
    {
      number = 993
      name   = "imap-ssl"
    },
    {
      number = 25
      name   = "smtp"
    },
    {
      number = 587
      name   = "smtp-tls"
    },
  ])
}
